#include "opencvqlabel.h"

OpencvQLabel::OpencvQLabel(QWidget *parent) :
    QLabel(parent),
    m_x(0), m_y(0), m_x_mat(0),m_y_mat(0)
{
}

void OpencvQLabel::showImage(Mat &toshow)
{
    matImage=toshow;
    if (matImage.data)
        this->setPixmap(QPixmap::fromImage(mat2qimage(matImage))/*.scaled(ui->labeImage->width(), ui->labeImage->height())*/);
}

void OpencvQLabel::mouseMoveEvent(QMouseEvent *ev)
{
    calcPosFromEvent(ev);
    emit Mouse_Pos();
}

void OpencvQLabel::mousePressEvent(QMouseEvent *ev)
{
    calcPosFromEvent(ev);
    emit Mouse_Pressed();
}

void OpencvQLabel::calcPosFromEvent(QMouseEvent *ev)
{
    m_x= ev->x();
    m_y= ev->y();
    if (matImage.data/*&&pixmap()->data*/)  // pytanie
    {
        m_x_mat=(int)((float)m_x*((float)matImage.cols/(float)width()));
        m_y_mat=(int)((float)m_y*((float)matImage.rows/(float)height()));
    }
}


QImage OpencvQLabel::mat2qimage(const cv::Mat &mat)
{
    // 8-bits unsigned, NO. OF CHANNELS=1
        if(mat.type()==CV_8UC1)
        {
            // Set the color table (used to translate colour indexes to qRgb values)
            QVector<QRgb> colorTable;
            for (int i=0; i<256; i++)
                colorTable.push_back(qRgb(i,i,i));
            // Copy input Mat
            const uchar *qImageBuffer = (const uchar*)mat.data;
            // Create QImage with same dimensions as input Mat
            QImage img(qImageBuffer, mat.cols, mat.rows, mat.step, QImage::Format_Indexed8);
            img.setColorTable(colorTable);
            return img;
        }
        // 8-bits unsigned, NO. OF CHANNELS=3
        if(mat.type()==CV_8UC3)
        {
            // Copy input Mat
            const uchar *qImageBuffer = (const uchar*)mat.data;
            // Create QImage with same dimensions as input Mat
            QImage img(qImageBuffer, mat.cols, mat.rows, mat.step, QImage::Format_RGB888);
            return img.rgbSwapped();
        }
        else
        {
            //qDebug() << "ERROR: Mat could not be converted to QImage.";
            return QImage();
        }
}
