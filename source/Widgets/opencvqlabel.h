#ifndef OPENCVQLABEL_H
#define OPENCVQLABEL_H

#include <QLabel>
#include <QMouseEvent>
#include "cv.h"
#include "highgui.h"
using namespace cv;

class OpencvQLabel : public QLabel
{
    Q_OBJECT
public:
    explicit OpencvQLabel(QWidget *parent = 0);
    
    void showImage(Mat & toshow);


    inline int x_lbl(){return m_x;}
    inline int y_lbl(){return m_y;}
    inline int x_mat(){return m_x_mat;}
    inline int y_mat(){return m_y_mat;}
    inline Vec2i pos_mat_vec(){return Vec2i(m_x_mat, m_y_mat);}
    inline Point2i pos_mat_pt(){return Point(m_x_mat, m_y_mat);}


signals:
    void Mouse_Pos();
    void Mouse_Pressed();
    //void Mouse_Left();

public slots:


private:
    int m_x, m_y;
    int m_x_mat, m_y_mat;

    Mat matImage;

    void mouseMoveEvent(QMouseEvent *ev);
    void mousePressEvent(QMouseEvent *ev);
    //void mouseLe
    void calcPosFromEvent(QMouseEvent *ev);

    QImage mat2qimage(const cv::Mat& mat);
};

#endif // OPENCVQLABEL_H
