#ifndef PARAMETERSMODEL_H
#define PARAMETERSMODEL_H

#include <QAbstractListModel>
//#include "procesparameter.h"
#include <QList>
#include <QVariant>

//extern int globalCounter;

class ParametersModel : public QAbstractTableModel
{
    Q_OBJECT

    QList<QList<QVariant> > parametersTable;

    bool headerSet;
public:
    //static int counter;

    explicit ParametersModel(QObject *parent = 0);
    ~ParametersModel();

    void addParameter(QString name, int par_min, int par_max);
    void addParameter(QString name, int par_min, int par_max, int init_value);

    QVariant data(const QModelIndex &index, int role= Qt::DisplayRole) const;
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role=Qt::EditRole);


    ParametersModel *clone(QObject *parent = 0);

    //int rowCount(const QModelIndex & parent = QModelIndex()){return parametersList->count();}
    //QVariant data( const QModelIndex & index, int role = Qt::DisplayRole ){return QVariant();}

signals:
    //void parameterChanged();
public slots:
    //void dataChangedSlot();
};

#endif // PARAMETERSMODEL_H
