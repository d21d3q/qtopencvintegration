#ifndef MATPROCESSITEM_H
#define MATPROCESSITEM_H

#include <QStandardItem>
#include <QTimerEvent>
#include "matprocesor.h"
#include "parametersmodel.h"

/*!
 * \brief The MatProcessItem class is used to store pointers to MatProcesor objects.
 */
class MatProcessItem :  public QObject, public QStandardItem
{
    Q_OBJECT
    //Q_DISABLE_COPY(MatProcessItem)

    MatProcesor *m_procesor;
    ParametersModel *paramModel;

public:
    explicit MatProcessItem(MatProcesor *procesor, QObject *parent = 0);
    //MatProcessItem(MatProcessItem &matprocessitem);
    ~MatProcessItem();

    ParametersModel *getParamModel();
    MatProcesor *getProcesor();
    MatProcessItem *clone(QObject *parent = 0);

private:
    void timerEvent(QTimerEvent *event);
signals:
    void parameterChanged();

public slots:
    void dataChangedSlot();

};

#endif // MATPROCESSITEM_H
