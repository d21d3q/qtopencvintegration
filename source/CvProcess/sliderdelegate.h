#ifndef SLIDERDELEGATE_H
#define SLIDERDELEGATE_H

#include <QStyledItemDelegate>
#include <QDebug>
#include "parametersmodel.h"

class SliderDelegate : public QStyledItemDelegate
{
    Q_OBJECT


public:
    explicit SliderDelegate(QObject *parent = 0);
    
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index);
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;


signals:
    
public slots:
    //void setModelDataSlot();
};

#endif // SLIDERDELEGATE_H
