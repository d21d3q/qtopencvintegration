#include "imageprocessoritem.h"

ImageProcessorItem::ImageProcessorItem(AbstractImageProcessor *procesor, QObject *parent):
    QObject(parent)
  , QStandardItem()
{
    m_procesor = procesor;

    if (procesor->getModelPtr())
    {
        paramModel = procesor->getModelPtr()->clone();

        setText(procesor->getName());

        startTimer(0);
        //connect(this->paramModel, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(dataChangedSlot()));
        //connect(this->paramModel, SIGNAL(parameterChanged()), this, SLOT(dataChangedSlot()));
        /*if (connect(paramModel, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(dataChangedSlot())))
            qDebug() << "Slot ustawiony";
        else
            qDebug() << "Slot nie ustawiony";*/

    } else
        paramModel=NULL;
}

ParametersModel *ImageProcessorItem::getParamModel()
{
    return paramModel;
}

AbstractImageProcessor *ImageProcessorItem::getProcesor()
{
    return m_procesor;
}

ImageProcessorItem *ImageProcessorItem::clone(QObject *parent)
{
    ImageProcessorItem *temp = new ImageProcessorItem(m_procesor, parent);
    temp->paramModel = paramModel->clone(temp);
    return temp;
}

void ImageProcessorItem::timerEvent(QTimerEvent *event)
{
    killTimer(event->timerId());
    connect(this->paramModel, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(dataChangedSlot()));
}

ImageProcessorItem::~ImageProcessorItem()
{
    //qDebug() << "Destruktor MatProcesItemu";
    if (paramModel)
    {
        delete paramModel;
        paramModel=NULL;
    }
}


void ImageProcessorItem::dataChangedSlot()
{
    emit parameterChanged();
}
