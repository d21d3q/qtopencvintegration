#ifndef MATPROCESOR_H
#define MATPROCESOR_H

//#include <QObject>
#include <QTime>
#include <QString>
#include <QDebug>

#include "cv.hpp"
using namespace cv;
//#include "procesparameter.h"
#include "parametersmodel.h"

/*!
 * \brief The AbstractImageProcessor is an abstract class which provides and object for processing Cv::Mat images.
 *
 *  This class should be subclassed with reimplemented function AbstractImageProcessor::processMat
 *  with included processing function.
 */
class AbstractImageProcessor
{
protected:
    //! pointer to ParametersModel object which will store parameters for image processing
    ParametersModel *paramModel;
    //! Timer for measuring time needed for processing image
    QTime m_Time;
    //! Mat container which contain last processed image
    Mat m_Procesedframe;
    //! if true processMat will measure time
    bool m_measureTime;
    //! contains last image processing duration
    int m_lastProcessDuration;
    //! contains name od function which proceses image
    QString m_name;


public:
    //! This is a constructor
    explicit AbstractImageProcessor();
    //! Copying constructor
    AbstractImageProcessor(AbstractImageProcessor &matprocesor);
    //! This is a destructor
    virtual ~AbstractImageProcessor();
    //virtual void processMat(Mat &src, Mat &dest);                                   //uses internal parameter list
    /*!
     * \brief processMat process image with included function.
     *  This function is pure virtual so that it must be implemented in subclass
     * \param src input image
     * \param dest ouptut image
     * \param model model containing parameters
     */
    virtual void processMat(Mat &src, Mat &dest, ParametersModel *model=NULL)=0;
    //! Returns last process duration
    inline int getLastProcDuration(){return m_lastProcessDuration;}
    //! Returns name of process
    inline QString getName(){return m_name;}
    //! Returns pointer to model containng parameters od proces
    inline ParametersModel *getModelPtr(){return paramModel;}
};

#endif // MATPROCESOR_H
