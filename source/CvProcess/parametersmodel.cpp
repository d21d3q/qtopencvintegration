#include "parametersmodel.h"

ParametersModel::ParametersModel(QObject *parent) :
    QAbstractTableModel(parent)
  , headerSet(false)
{

   /* instanceNumber=globalCounter;
    qDebug() << "Powstala " << instanceNumber << " instancja parametermodelu";
    globalCounter++;*/
}

ParametersModel::~ParametersModel()
{
}

void ParametersModel::addParameter(QString name, int par_min, int par_max)
{
    if (par_max<par_min)
        par_max=par_min;

    int value = par_min;

    int last = parametersTable.count();
    parametersTable.append(QList<QVariant>());
    parametersTable[last].append(name);
    parametersTable[last].append(par_min);
    parametersTable[last].append(par_max);
    parametersTable[last].append(value);

    if (!headerSet)
    {
        setHeaderData(0, Qt::Horizontal, "Name");
        setHeaderData(1, Qt::Horizontal, "Min");
        setHeaderData(2, Qt::Horizontal, "Max");
        setHeaderData(3, Qt::Horizontal, "Value");
        headerSet=true;
    }
}

void ParametersModel::addParameter(QString name, int par_min, int par_max, int init_value)
{
    if (par_max<par_min)
        par_max=par_min;


    int last = parametersTable.count();
    parametersTable.append(QList<QVariant>());
    parametersTable[last].append(name);
    parametersTable[last].append(par_min);
    parametersTable[last].append(par_max);
    parametersTable[last].append(init_value);

    if (!headerSet)
    {
        setHeaderData(0, Qt::Horizontal, "Name");
        setHeaderData(1, Qt::Horizontal, "Min");
        setHeaderData(2, Qt::Horizontal, "Max");
        setHeaderData(3, Qt::Horizontal, "Value");
        headerSet=true;
    }
}



QVariant ParametersModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    /*if (index.row() >= parametersTable.size())
        return QVariant();
    if (index.column() >= parametersTable[index.row()].size())
        return QVariant();*/

    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        //qDebug() << "pm data: " << parametersTable[index.row()][index.column()] << " index: " << index.row() << " " << index.column();
        return parametersTable[index.row()][index.column()];
    }
    else
        return QVariant();
}

int ParametersModel::rowCount(const QModelIndex &parent) const
{
    return parametersTable.count();
}

int ParametersModel::columnCount(const QModelIndex &parent) const
{
    return 4;
}

Qt::ItemFlags ParametersModel::flags(const QModelIndex &index) const
{
    if (index.column()==3)
        return Qt::ItemIsEditable | Qt::ItemIsSelectable | Qt::ItemIsEnabled;
    else
        return Qt::ItemIsSelectable /*| Qt::ItemIsEnabled*/;
}

bool ParametersModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(index.isValid() || role == Qt::EditRole || value.isValid())
    {
        if (index.column()==3)
            if(value.toInt()>=parametersTable[index.row()][1].toInt()
               &&value.toInt()<=parametersTable[index.row()][2].toInt()
               &&value.toInt()!=parametersTable[index.row()][index.column()].toInt())
            {
                parametersTable[index.row()][index.column()]=value;
                //qDebug() << "sd: " << value << ", " << parametersTable[index.row()][index.column()];
                emit dataChanged(index,index);
                //emit parameterChanged();
                return true;
            }
    }

    return false;
}

ParametersModel *ParametersModel::clone(QObject *parent)
{
    ParametersModel *temp = new ParametersModel(parent);

    foreach(QList<QVariant> a, parametersTable)
    {
        temp->parametersTable.append(QList<QVariant>());
        foreach(QVariant v, a){
            temp->parametersTable.last().append(v);
        }
    }


    return temp;
}
