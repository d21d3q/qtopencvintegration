#include "sliderdelegate.h"

SliderDelegate::SliderDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
  //, m_editor(NULL)

{
}
QWidget *SliderDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index)
{
    QSlider *slider = new QSlider(Qt::Horizontal, parent);
    slider->setMinimum(index.model()->data(index.model()->index(index.row(), index.column()-2)).toInt());
    slider->setMaximum(index.model()->data(index.model()->index(index.row(), index.column()-1)).toInt());

    //connect(slider, SIGNAL(valueChanged(int)), static_cast<ParametersModel *>(index.model()), SLOT(dataChangedSlot()));

    /*m_editor=slider;
    m_index=index;

    connect(slider, SIGNAL(valueChanged(int)), this, SLOT(setModelDataSlot()));*/

    return slider;
}

void SliderDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    int value = index.model()->data(index).toInt();

    QSlider * slider = static_cast<QSlider *>(editor);
    slider->setValue(value);
    //qDebug() << "sed: value: " << value;
}

void SliderDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    editor->setGeometry(option.rect);
}


void SliderDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QSlider * slider = static_cast<QSlider *>(editor);
    int value = slider->value();
    model->setData(index, value);
    //qDebug() << "smd: " << value;
}

/*void SliderDelegate::setModelDataSlot()
{
    setModelData(m_editor, m_index.model(), m_index);
}*/
