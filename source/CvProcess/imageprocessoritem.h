#ifndef MATPROCESSITEM_H
#define MATPROCESSITEM_H

#include <QStandardItem>
#include <QTimerEvent>
#include "abstractimageprocessor.h"
#include "parametersmodel.h"

/*!
 * \brief The ImageProcessorItem class is used to store pointers to AbstractImageProcessor objects.
 */
class ImageProcessorItem :  public QObject, public QStandardItem
{
    Q_OBJECT
    //Q_DISABLE_COPY(ImageProcessorItem)

    AbstractImageProcessor *m_procesor;
    ParametersModel *paramModel;

public:
    explicit ImageProcessorItem(AbstractImageProcessor *procesor, QObject *parent = 0);
    //ImageProcessorItem(ImageProcessorItem &matprocessitem);
    ~ImageProcessorItem();

    ParametersModel *getParamModel();
    AbstractImageProcessor *getProcesor();
    ImageProcessorItem *clone(QObject *parent = 0);

private:
    void timerEvent(QTimerEvent *event);
signals:
    void parameterChanged();

public slots:
    void dataChangedSlot();

};

#endif // MATPROCESSITEM_H
