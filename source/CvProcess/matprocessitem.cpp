#include "matprocessitem.h"

MatProcessItem::MatProcessItem(MatProcesor *procesor, QObject *parent):
    QObject(parent)
  , QStandardItem()
{
    m_procesor = procesor;

    if (procesor->getModelPtr())
    {
        paramModel = procesor->getModelPtr()->clone();

        setText(procesor->getName());

        startTimer(0);
        //connect(this->paramModel, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(dataChangedSlot()));
        //connect(this->paramModel, SIGNAL(parameterChanged()), this, SLOT(dataChangedSlot()));
        /*if (connect(paramModel, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(dataChangedSlot())))
            qDebug() << "Slot ustawiony";
        else
            qDebug() << "Slot nie ustawiony";*/

    } else
        paramModel=NULL;
}

ParametersModel *MatProcessItem::getParamModel()
{
    return paramModel;
}

MatProcesor *MatProcessItem::getProcesor()
{
    return m_procesor;
}

MatProcessItem *MatProcessItem::clone(QObject *parent)
{
    MatProcessItem *temp = new MatProcessItem(m_procesor, parent);
    temp->paramModel = paramModel->clone(temp);
    return temp;
}

void MatProcessItem::timerEvent(QTimerEvent *event)
{
    killTimer(event->timerId());
    connect(this->paramModel, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(dataChangedSlot()));
}

MatProcessItem::~MatProcessItem()
{
    //qDebug() << "Destruktor MatProcesItemu";
    if (paramModel)
    {
        delete paramModel;
        paramModel=NULL;
    }
}


void MatProcessItem::dataChangedSlot()
{
    emit parameterChanged();
}
