#include "matprocesor.h"

MatProcesor::MatProcesor() :
    /*QObject(parent),*/
    m_measureTime(true),
    m_lastProcessDuration(0)
{
    m_name="Empty";
    paramModel = new ParametersModel();
}

MatProcesor::MatProcesor(MatProcesor &matprocesor):
    m_measureTime(true)
  , m_lastProcessDuration(0)
{
    qDebug() << "konstruktor kopiujacy matprocesora";

    if (!matprocesor.getModelPtr())
    {
        paramModel= matprocesor.getModelPtr()->clone();
    } else
        paramModel=NULL;

    m_name=matprocesor.m_name;
}

MatProcesor::~MatProcesor()
{
    //qDebug() << "Destruktor matprocesora";
    //qDeleteAll(*m_parameters)
    if (paramModel)
    {
        delete paramModel;
        paramModel=NULL;
    }


}

/*void MatProcesor::processMat(Mat &src, Mat &dest)
{
    if (paramModel==NULL)     //missing parameters list
        return;

    if (m_measureTime)
        m_Time.start();
    dest=src;
    if (m_measureTime)
        m_lastProcessDuration = m_Time.elapsed();
}
*/
/*void MatProcesor::processMat(Mat &src, Mat &dest, ParametersModel *model)
{
    if (model==NULL)     //missing parameters list
        return;

    if (m_measureTime)
        m_Time.start();
    dest=src;
    if (m_measureTime)
        m_lastProcessDuration = m_Time.elapsed();
}*/
