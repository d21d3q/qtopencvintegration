#ifndef PROCGAUSIANBLUR_H
#define PROCGAUSIANBLUR_H

#include "../matprocesor.h"

class ProcGausianBlur : public MatProcesor
{

    int m_kernelX;
    int m_kernelY;
    int m_sigmaX;

public:
    explicit ProcGausianBlur();
    ~ProcGausianBlur();
    virtual void processMat(Mat &src, Mat &dst, ParametersModel *model=NULL);
    //virtual void processMat(Mat &src, Mat &dst, QList<ProcesParameter *> *param);



//signals:

//public slots:
    
};

#endif // PROCGAUSIANBLUR_H
