#include "procthreshold.h"

ProcThreshold::ProcThreshold():
    MatProcesor()
, thresh(127)
{
    m_name = "Threshold";

    paramModel->addParameter("Threshold", 0, 255, 127);
}

void ProcThreshold::processMat(Mat &src, Mat &dst, ParametersModel *model)
{
    if (model==NULL)
        model=paramModel;

    //for (int i=0; i<4; i++)
        //qDebug() << "threshold: " << model->data(model->index(0,i));

    //thresh=paramModel->QAbstractItemModel::data(paramModel->index(0,3)).toInt();
    thresh=model->data(model->index(0,3)).toInt();

    if (m_measureTime)
        m_Time.start();
    threshold(src, dst, thresh, 255, THRESH_BINARY );
    if (m_measureTime)
        m_lastProcessDuration = m_Time.elapsed();
}
