#include "procgausianblur.h"

ProcGausianBlur::ProcGausianBlur() :
    MatProcesor(),
    m_kernelX(1),
    m_kernelY(1),
    m_sigmaX(1)

{
    m_name = "Gausian Blur";

    paramModel->addParameter("Kernl x *2+e1", 0, 10);

    paramModel->addParameter("Kernel y *2+1", 0, 10);

    paramModel->addParameter("Sigma x /100", 1, 5000);
}

ProcGausianBlur::~ProcGausianBlur()
{
    qDebug() << "Destruktr ProcGausianBlur";
}

void ProcGausianBlur::processMat(Mat &src, Mat &dst, ParametersModel *model)
{
    if (model==NULL)
        model=paramModel;

    m_kernelX=model->data(model->index(0,3), Qt::DisplayRole).toInt();
    m_kernelY=model->data(model->index(1,3), Qt::DisplayRole).toInt();
    m_sigmaX=model->data(model->index(2,3), Qt::DisplayRole).toInt();

    if (m_measureTime)
        m_Time.start();
    GaussianBlur(src, dst, Size(m_kernelX*2 + 1, m_kernelY*2 + 1), static_cast<double>(m_sigmaX/100));
    if (m_measureTime)
        m_lastProcessDuration = m_Time.elapsed();
}

/*void ProcGausianBlur::processMat(Mat &src, Mat &dst, QList<ProcesParameter *> *param)
{
    if (param->count()==3)
    {
        if (m_measureTime)
            m_Time.start();
        GaussianBlur(src, dst, Size(param->at(0)->getValue()*2 + 1, param->at(1)->getValue()*2 + 1), static_cast<double>(param->at(2)->getValue()/100));
        if (m_measureTime)
            m_lastProcessDuration = m_Time.elapsed();
    }
}*/
