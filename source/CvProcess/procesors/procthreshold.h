#ifndef PROCTHRESHOLD_H
#define PROCTHRESHOLD_H

#include "../matprocesor.h"
//#include "QAbstractItemModel"

class ProcThreshold : public MatProcesor
{
    int thresh;
public:
    ProcThreshold();
    virtual void processMat(Mat &src, Mat &dst, ParametersModel *model=NULL);
    //virtual QVariant
};

#endif // PROCTHRESHOLD_H
